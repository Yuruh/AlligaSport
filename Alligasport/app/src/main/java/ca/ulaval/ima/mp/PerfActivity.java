package ca.ulaval.ima.mp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PerfActivity extends AppCompatActivity implements PerfFragment.OnListFragmentInteractionListener{
    Session session = null;


    public void initExerciseSelectionFragment() {
        this.setTitle("Enter your performance :");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.perf_fragment_place, PerfFragment.newInstance(session));
        fragmentTransaction.commit();
    }

    public Session getSession()
    {
        return session;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perf);
        Intent intent = getIntent();
        session = intent.getExtras().getParcelable("SESSION");
        initExerciseSelectionFragment();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.validate_session, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.validate:
                PerfFragment fragment = (PerfFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.perf_fragment_place);
                HashMap sessionPerformance = fragment.getSessionPerformance();
                JSONObject perfs = FileManager.getInstance().getPerfs();
                try {
                    JSONArray exerciseArray = perfs.getJSONArray("exercises");
                    Iterator it = sessionPerformance.entrySet().iterator();
                    while (it.hasNext())
                    {
                        Map.Entry pair = (Map.Entry)it.next();
                        boolean written = false;
                        for (int i = 0; i < exerciseArray.length(); ++i)
                        {
                            String tmp = ((JSONObject)exerciseArray.get(i)).getString("exercise_name");
                            if (tmp.matches(pair.getKey().toString())) {
                                JSONArray perfArray = ((JSONObject) exerciseArray.get(i)).getJSONArray("perf");
                                perfArray.put(pair.getValue().toString());
                                written = true;
                            }
                        }
                        if (!written)
                        {
                            JSONObject newExercisePerformance = new JSONObject();
                            newExercisePerformance.put("exercise_name", pair.getKey().toString());
                            JSONArray perfArray = new JSONArray();
                            perfArray.put(pair.getValue().toString());
                            newExercisePerformance.put("perf", perfArray);
                            exerciseArray.put(newExercisePerformance);
                        }
                        it.remove();
                    }
                    FileManager.getInstance().setAndRewritePerfFile(perfs);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Toast.makeText(this, "Your performance has been saved.",
                        Toast.LENGTH_LONG).show();
                finish();
                return true;
        }
        return true;
    }


    @Override
    public void onListFragmentInteraction(final Exercise item) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Enter your performance :");

// Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (input.getText().toString().matches(""))
                    return;
                PerfFragment fragment = (PerfFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.perf_fragment_place);
                item.setPerf(Integer.parseInt(input.getText().toString()));
                fragment.modifyPerfMap(item.getName(), Integer.parseInt(input.getText().toString()));
                fragment.notifyDataChanged();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }
}
