package ca.ulaval.ima.mp;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by yuruh on 04/05/18.
 */

public class Decorator extends RecyclerView.ItemDecoration {
    public Decorator() {
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = 15;
        outRect.top = 15;
        outRect.left = 15;
        outRect.right = 15;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
/*        Paint paint = new Paint();
        paint.setColor(ContextCompat.getColor(parent.getContext(), R.color.colorPrimary));
        paint.setStyle(Paint.Style.FILL);

        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        final int itemCount = parent.getChildCount();
        for (int i = 0; i < itemCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getRight() + params.rightMargin;
            c.drawLine(left,top,left,bottom, paint);
        }*/
    }

}
