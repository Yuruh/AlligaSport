package ca.ulaval.ima.mp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;


public class ExercisePerfRecyclerViewAdapter extends RecyclerView.Adapter<ExercisePerfRecyclerViewAdapter.ViewHolder> {

    private final List<ExercisePerfFragment.ExercisePerf> mValues;

    public ExercisePerfRecyclerViewAdapter(List<ExercisePerfFragment.ExercisePerf> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_exerciseperf, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mPerfView.setText(holder.mItem.exercisePerf);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPerfView;
        public ExercisePerfFragment.ExercisePerf mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPerfView = (TextView) view.findViewById(R.id.exercise_perf_number);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPerfView.getText() + "'";
        }
    }
}
