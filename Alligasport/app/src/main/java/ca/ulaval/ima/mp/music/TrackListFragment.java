package ca.ulaval.ima.mp.music;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import ca.ulaval.ima.mp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TrackListFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private OnTrackClickListener mTrackListener;
    private String playlistId;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TrackListFragment() {}

    @SuppressWarnings("unused")
    public static TrackListFragment newInstance(String id) {
        TrackListFragment fragment = new TrackListFragment();
        Bundle args = new Bundle();
        args.putString("ID", id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playlistId = getArguments().getString("ID");
/*        try {
            mTrackListener = (OnTrackClickListener) getParentFragment();
        }
        catch (ClassCastException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_list, container, false);
        MusicHandler.getInstance().setupMediaController(getView(), getContext());
        if (view instanceof RecyclerView) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    MusicHandler.getInstance().showMediaController(getView());
                    return false;
                }
            });
            Context context = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new TrackRecyclerViewAdapter(new ArrayList<SpotifyTrack>(), mListener, mTrackListener));

            SpotifyApi.getInstance().getPlaylistTracks(playlistId, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    JSONObject object = null;
                    try {
                        object = new JSONObject(response.body().string());

                        final List<SpotifyTrack> items = new ArrayList<>();

                        JSONArray tracks = object.getJSONArray("items");
                        for (int i = 0; i < tracks.length(); i++) {
                            items.add(new SpotifyTrack(tracks.getJSONObject(i).getJSONObject("track")));
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.setAdapter(new TrackRecyclerViewAdapter(items, mListener, mTrackListener));
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        MusicHandler.getInstance().hideMediaController();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(SpotifyTrack item);
    }

    public interface OnTrackClickListener {
        void OnTrackClick(SpotifyTrack track, List<SpotifyTrack> spotifyTracks);
    }
}
