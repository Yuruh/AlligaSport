package ca.ulaval.ima.mp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ca.ulaval.ima.mp.music.AddPlaylistDialogFragment;
import ca.ulaval.ima.mp.music.MusicFragment;
import ca.ulaval.ima.mp.music.PlaylistFragment;
import ca.ulaval.ima.mp.music.SpotifyTrack;
import ca.ulaval.ima.mp.music.TrackListFragment;

public class MainActivity extends AppCompatActivity implements
        MusicFragment.OnMusicFragmentInteractionListener,
        TrackListFragment.OnListFragmentInteractionListener,
        ListSessionFragment.OnListFragmentInteractionListener,
        CreateSessionFragment.OnFragmentInteractionListener,
        PlaylistFragment.OnListFragmentInteractionListener,
        AddPlaylistDialogFragment.OnFragmentInteractionListener,
        DuringSessionFragment.OnListFragmentInteractionListener,
        PerfFragment.OnListFragmentInteractionListener,
        ViewPerfsFragment.OnListFragmentInteractionListener,
        FragmentManager.OnBackStackChangedListener
{
    private Fragment musicFragment = new MusicFragment();
    private Fragment sessionFragment = new ListSessionFragment();
    private Fragment duringSessionFragment = null;
    private Fragment createSessionFragment = new CreateSessionFragment();
    private Fragment speedFragment = new SpeedFragment();
    private Fragment checkPerfFragment = new ViewPerfsFragment();
    private boolean duringSession = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE); //OPEN
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragmentTransaction.replace(R.id.main_content, sessionFragment).commit();

                    //To stay focused on the current session
                    fragmentTransaction = fragmentManager.beginTransaction();
                    if (duringSession)
                        fragmentTransaction.replace(R.id.main_content, duringSessionFragment)
                                .addToBackStack(null);
                    else
                        duringSessionFragment = null;
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_add_session:
                    fragmentTransaction.replace(R.id.main_content, createSessionFragment).commit();
                    return true;
                case R.id.check_performance:
                    checkPerfFragment = new ViewPerfsFragment();
                    fragmentTransaction.replace(R.id.main_content, checkPerfFragment).commit();
                    return true;
                case R.id.navigation_speed:
                    fragmentTransaction.replace(R.id.main_content, speedFragment).commit();
                    return true;
                case R.id.navigation_music:
                    fragmentTransaction.replace(R.id.main_content, musicFragment, "MusicFragment").commit();
                    return true;
            }
            return false;
        }
    };

    public void endDuringSession() {
        duringSession = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.getFragments().isEmpty())
            navigation.setSelectedItemId(R.id.navigation_home);

        FileManager.getInstance(this).updateJsonFiles();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(getSupportFragmentManager().getBackStackEntryCount() > 0);
    }


    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("MusicFragment");
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, intent);
//        Log.wtf("azer", String.valueOf(requestCode));
    }

    @Override
    public void onListFragmentInteraction(SpotifyTrack item) {

    }

    @Override
    public void onListFragmentInteraction(Session session) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        DuringSessionFragment duringSessionFragment = DuringSessionFragment.newInstance(session);
//        fragmentManager.executePendingTransactions();
//        sessionFragment = DuringSessionFragment.newInstance(session);
        duringSession = true;
        duringSessionFragment = DuringSessionFragment.newInstance(session);
        fragmentTransaction.replace(R.id.main_content, duringSessionFragment).addToBackStack(null).commit();
    }

    @Override
    public void onListFragmentInteraction(Pair<String, String> playlistIdName) {
        MusicFragment fragment = (MusicFragment) getSupportFragmentManager().findFragmentByTag("MusicFragment");
        if (fragment != null)
            fragment.setTrackListFragment(TrackListFragment.newInstance(playlistIdName.first), playlistIdName.second);
    }


    @Override
    public void onListFragmentInteraction(ViewPerfsFragment.ExerciseName item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        checkPerfFragment = ExercisePerfFragment.newInstance(item.exerciseName);
        fragmentTransaction.replace(R.id.main_content, checkPerfFragment).addToBackStack(null).commit();
    }

    @Override
    public void onListFragmentInteraction(Exercise item) {

    }
}
