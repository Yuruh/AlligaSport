package ca.ulaval.ima.mp;

/**
 * Created by cot_s on 18-04-11.
 */

public enum ExerciseType {
    WEIGHT, CARDIO
}
