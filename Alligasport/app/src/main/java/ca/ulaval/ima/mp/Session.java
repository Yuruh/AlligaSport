package ca.ulaval.ima.mp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuruh on 05/04/18.
 */

public class Session implements Parcelable {
    private List<Exercise>  exercises = new ArrayList<>();
    private String          name;
    private int             id;

    protected Session(Parcel in) {
        id = in.readInt();
        name = in.readString();
        exercises = in.createTypedArrayList(Exercise.CREATOR);
    }


    public Session() {
        this.name = null;
        this.id = 0;
        this.exercises = new ArrayList<Exercise>();
    }

    public Session(Session session)
    {
        this.name = session.name;
        this.id = session.id;
        for (Exercise exercise : session.getExercises())
            this.exercises.add(new Exercise(exercise));
    }

    public static final Creator<Session> CREATOR = new Creator<Session>() {
        @Override
        public Session createFromParcel(Parcel in) {
            return new Session(in);
        }

        @Override
        public Session[] newArray(int size) {
            return new Session[size];
        }
    };

    public String getName() {
        return this.name;
    }

    public Session(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
            name = jsonObject.getString("name");
            JSONArray jsonExercices = jsonObject.getJSONArray("exercises");
            for (int i = 0; i < jsonExercices.length(); i++) {
                exercises.add(new Exercise(jsonExercices.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeTypedList(exercises);
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonSession = new JSONObject();
        jsonSession.put("name", this.name);
        jsonSession.put("id", this.id);
        return jsonSession;
    }
}
