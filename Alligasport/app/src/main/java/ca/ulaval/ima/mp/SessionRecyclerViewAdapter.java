package ca.ulaval.ima.mp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class SessionRecyclerViewAdapter extends RecyclerView.Adapter<SessionRecyclerViewAdapter.ViewHolder> {

    private final List<Session> mValues;
    private final ListSessionFragment.OnListFragmentInteractionListener mListener;

    public SessionRecyclerViewAdapter(List<Session> items, ListSessionFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_session, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mSessionNameView.setText(mValues.get(position).getName());
        holder.mExerciseQtyView.setText(Integer.toString(mValues.get(position).getExercises().size()) + " exercises");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mSessionNameView;
        public final TextView mExerciseQtyView;
        public Session mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mSessionNameView = view.findViewById(R.id.session_name_text);
            mExerciseQtyView = view.findViewById(R.id.exercise_qty_text);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mExerciseQtyView.getText() + "'";
        }
    }
}
