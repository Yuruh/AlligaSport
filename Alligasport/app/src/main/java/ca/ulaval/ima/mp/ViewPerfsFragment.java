package ca.ulaval.ima.mp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class ViewPerfsFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;

    public ViewPerfsFragment() {
    }

    @SuppressWarnings("unused")
    public static ViewPerfsFragment newInstance() {
        ViewPerfsFragment fragment = new ViewPerfsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ArrayList<ExerciseName> createExercisesList(JSONArray exercises) throws JSONException {
        ArrayList<ExerciseName> list = new ArrayList<>();
        for (int i = 0; i < exercises.length(); ++i)
        {
            list.add(new ExerciseName(exercises.getJSONObject(i).getString("exercise_name")));
        }
        return list;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_perfs_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            this.getActivity().setTitle("Your Performances");
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            try {
                JSONArray exercisesArray = FileManager.getInstance().getPerfs().getJSONArray("exercises");
                recyclerView.setAdapter(new ViewPerfsRecyclerViewAdapter(createExercisesList(exercisesArray), mListener));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(ExerciseName item);
    }

    public class ExerciseName {
        public final String exerciseName;

        public ExerciseName(String name)
        {
            exerciseName = name;
        }
    }
}
