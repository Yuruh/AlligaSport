package ca.ulaval.ima.mp.music;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuruh on 30/03/18.
 */

public class SpotifyTrack {
    public String name;
    public String id;
    public String uri;
    public String album;
    public String artist;
    public boolean isPlaying;

    public SpotifyTrack(JSONObject json) throws JSONException {
        name = json.getString("name");
        id = json.getString("id");
        uri = json.getString("uri");
        album = json.getJSONObject("album").getString("name");
        artist = json.getJSONArray("artists").getJSONObject(0).getString("name");
        isPlaying = false;
    }
}
