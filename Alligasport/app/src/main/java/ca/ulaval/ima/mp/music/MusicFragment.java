package ca.ulaval.ima.mp.music;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ca.ulaval.ima.mp.R;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.SpotifyPlayer;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MusicFragment.OnMusicFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MusicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MusicFragment extends Fragment implements
        TrackListFragment.OnTrackClickListener
{

    private static final String CLIENT_ID = "a54f7cc11da74fd7b990d6722027fc8b";

    private static final String REDIRECT_URI = "http://localhost:8888/callback/";

    private static final int SPOTIFY_RESULT_CODE = 3214;

    private OnMusicFragmentInteractionListener mListener;

    public MusicFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MusicFragment.
     */
    public static MusicFragment newInstance(String param1, String param2) {
        MusicFragment fragment = new MusicFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (MusicHandler.getInstance().getPlayer() == null) {

            // The only thing that's different is we added the 5 lines below.
            AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
            builder.setScopes(new String[]{"user-read-private", "streaming", "user-library-read", "user-top-read", "playlist-modify-public",
                    "playlist-modify-private"});
            AuthenticationRequest request = builder.build();

            AuthenticationClient.openLoginActivity(getActivity(), SPOTIFY_RESULT_CODE, request);
        }
        else {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            TrackListFragment fragment = new TrackListFragment();

            PlaylistFragment playlistFragment = new PlaylistFragment();

//            fragmentTransaction.replace(R.id.track_list_container, fragment).commit();
            fragmentTransaction.replace(R.id.track_list_container, playlistFragment).commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == SPOTIFY_RESULT_CODE) {

            final AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            if (response.getType() == AuthenticationResponse.Type.TOKEN) {
                SpotifyApi.getInstance().setToken(response.getAccessToken());
                SpotifyApi.getInstance().setupUserId();
                Config playerConfig = new Config(getActivity(), response.getAccessToken(), CLIENT_ID);
                Spotify.getPlayer(playerConfig, this, new SpotifyPlayer.InitializationObserver() {
                    @Override
                    public void onInitialized(SpotifyPlayer spotifyPlayer) {
                        MusicHandler.getInstance().setPlayer(spotifyPlayer);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

//                        TrackListFragment fragment = new TrackListFragment();
                        PlaylistFragment playlistFragment = new PlaylistFragment();

//                        fragmentTransaction.replace(R.id.track_list_container, fragment).commit();
                        fragmentTransaction.replace(R.id.track_list_container, playlistFragment).commit();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
                    }
                });
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Music");
        View view = inflater.inflate(R.layout.fragment_music, container, false);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMusicFragmentInteractionListener) {
            mListener = (OnMusicFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void OnTrackClick(final SpotifyTrack track, final List<SpotifyTrack> spotifyTracks) {
        final Player player = MusicHandler.getInstance().getPlayer();

        player.playUri(new Player.OperationCallback() {
            @Override
            public void onSuccess() {
                MusicHandler.getInstance().clearQueue();
                for (SpotifyTrack nextTrack : spotifyTracks) {
                    MusicHandler.getInstance().addToQueue(nextTrack);
                }
            }

            @Override
            public void onError(Error error) {

            }
        }, track.uri, 0, 0);
    }

    public void setTrackListFragment(Fragment musicFragment, String name) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        TrackListFragment trackListFragment = TrackListFragment.newInstance(playlistIdName.first);
//        fragmentManager.executePendingTransactions();
        getActivity().setTitle(name);
        fragmentTransaction.replace(R.id.track_list_container, musicFragment).addToBackStack(null).commit();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMusicFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
