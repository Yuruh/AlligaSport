package ca.ulaval.ima.mp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

public class SpeedFragment extends Fragment implements LocationListener {

    Location location;
    LocationManager locationManager;
    LocationProvider provider;

    Chronometer chronometer;
    boolean startChrono = false;
    boolean pauseChrono = false;
    long timeOnPause = 0;
    private float tick = 0;

    private float currentSpeed = 0;
    private float averageSpeed = 0;

    TextView averageSpeedTextView;
    TextView currentSpeedTextView;

    public SpeedFragment() {
    }

    public static SpeedFragment newInstance() {
        return new SpeedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            return;
        }

        provider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000, 1, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Speed");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_speed, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        averageSpeedTextView = (TextView) view.findViewById(R.id.averageSpeedTextView);
        currentSpeedTextView = (TextView) getView().findViewById(R.id.currentSpeedTextView);

        chronometer = (Chronometer) view.findViewById(R.id.simpleChronometer);
        chronometer.setOnChronometerTickListener(
                new Chronometer.OnChronometerTickListener() {
                    public void onChronometerTick(Chronometer arg0) {
                        averageSpeed = Math.round((((averageSpeed * tick) + currentSpeed) / (tick + 1)) * (float) 100.0) / (float) 100.0;
                        tick++;

                        String speedText = "Average speed: " + String.valueOf(averageSpeed) + " km/h";
                        averageSpeedTextView.setText(speedText);
                    }
                }
        );

        final Button chronoStartBtn = (Button) view.findViewById(R.id.chronoStartBtn);
        final Button chronoResetBtn = (Button) view.findViewById(R.id.chronoResetBtn);

        // Start or pause or resume the chronometer
        chronoStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!startChrono) {
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();

                    startChrono = true;
                    chronoStartBtn.setText("Pause");
                } else {
                    if (!pauseChrono) {
                        chronoStartBtn.setText("Resume");
                        chronometer.stop();
                        timeOnPause = SystemClock.elapsedRealtime();
                    } else {
                        chronoStartBtn.setText("Pause");
                        chronometer.start();
                        chronometer.setBase(chronometer.getBase() + SystemClock.elapsedRealtime() - timeOnPause);
                    }
                    pauseChrono = !pauseChrono;
                }
            }
        });

        // Reset the chronometer, currentSpeed & averageSpeed
        chronoResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.stop();
                chronoStartBtn.setText("Start");
                startChrono = false;
                pauseChrono = false;
                averageSpeed = 0;
                tick = 0;
                currentSpeedTextView.setText(R.string.current_speed);
                averageSpeedTextView.setText(R.string.average_speed);
            }
        });
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            this.location = location;
            currentSpeed = Math.round(location.getSpeed() * (float) 3.6 * (float) 100.0) / (float) 100.0;
            String curSpeed = "Current speed: " + String.valueOf(currentSpeed) + " km/h";
            currentSpeedTextView.setText(curSpeed);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
