package ca.ulaval.ima.mp;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExerciseSelectionActivity
        extends AppCompatActivity
        implements ExerciseSelectionFragment.OnListFragmentInteractionListener{

    Session session = null;


    public void initExerciseSelectionFragment() {
        this.setTitle("Choose the exercises :");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ExerciseSelectionFragment fragment = new ExerciseSelectionFragment();
        try {
            fragment.initializeList();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ;
        fragmentTransaction.add(R.id.session_creation_fragment_place, fragment);
        fragmentTransaction.commit();
    }

    public Session getSession()
    {
        return session;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice_selection);
        Intent intent = getIntent();
        session = intent.getParcelableExtra("parcelableSession");
        initExerciseSelectionFragment();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.validate_session, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.validate:
                ArrayList<Exercise> tmp
                        = new ArrayList<Exercise>();
                ExerciseSelectionFragment fragment = (ExerciseSelectionFragment)getSupportFragmentManager()
                        .findFragmentById(R.id.session_creation_fragment_place);
                JSONArray exerciseArray = new JSONArray();
                for (Exercise exerciseItem : fragment.getExerciseItemsList()) {
                    if (exerciseItem.selected) {
                        try {
                            exerciseArray.put(exerciseItem.getJsonObject());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                        tmp.add(exerciseItem);
                }

                JSONObject sessions = FileManager.getInstance().getSessions();
                try {
                    JSONObject newSession = session.getJsonObject();
                    newSession.put("exercises", exerciseArray);
                    sessions.getJSONArray("sessions").put(newSession);
                    FileManager.getInstance().setAndRewriteSessionFile(sessions);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(this, "Session was created.",
                        Toast.LENGTH_LONG).show();
                finish();
                return true;
        }
        return true;
    }


    @Override
    public void onListFragmentInteraction(Exercise item) {

    }
}
