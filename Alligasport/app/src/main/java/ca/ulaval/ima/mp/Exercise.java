package ca.ulaval.ima.mp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuruh on 05/04/18.
 */

public class Exercise implements Parcelable {
    private String name;
    private Integer id;
    private String description;
    private short reps;
    private Integer restDuration;
    private ExerciseType exerciseType;
    //private MuscleUsed muscleUsed;
    private Integer iconId = R.drawable.ic_bench_press; // todo change default icon
    public boolean selected;
    private Integer perf = 0;

    public Exercise(String name, Integer id, String description,
                    short reps, Integer restDuration, ExerciseType exerciseType) {
        this.name = name;
        this.id = id;
        this.description = description;
        this.reps = reps;
        this.restDuration = restDuration;
        this.exerciseType = exerciseType;
        this.selected = false;
        this.setIconIdFromName();
    }

    public Exercise(Exercise item) {
        this.name = item.name;
        this.id = item.id;
        this.description = item.description;
        this.reps = item.reps;
        this.restDuration = item.restDuration;
        this.exerciseType = item.exerciseType;
    }

    public Exercise(JSONObject jsonObject) {
        try {
            name = jsonObject.getString("name");
            id = jsonObject.getInt("id");
            restDuration = jsonObject.getInt("rest_duration");
            reps = (short) jsonObject.getInt("reps");
            description = jsonObject.getString("description");
            exerciseType = ExerciseType.WEIGHT;//todo jsonObject.getString("type");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.setIconIdFromName();
    }

    public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonExercise = new JSONObject();
        jsonExercise.put("name", name);
        jsonExercise.put("id", id);
        jsonExercise.put("description", description);
        jsonExercise.put("reps", reps);
        jsonExercise.put("rest_duration", restDuration);
        jsonExercise.put("type", exerciseType.toString());
        return jsonExercise;
    }

    protected Exercise(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        description = in.readString();
        reps = (short) in.readInt();
        if (in.readByte() == 0) {
            restDuration = null;
        } else {
            restDuration = in.readInt();
        }
        String tmp = in.readString();
        if (tmp != null)
            exerciseType = ExerciseType.valueOf(tmp);
        this.setIconIdFromName();
    }

    //todo this but mieux
    private void setIconIdFromName() {
        switch (name) {
            case "Bench Press":
                iconId = R.drawable.ic_bench_press;
                break;
            case "Abs":
                iconId = R.drawable.ic_abs;
                break;
            case "Fentes":
                iconId = R.drawable.ic_fente;
                break;
        }
    }

    public void setPerf(Integer perf)
    {
        this.perf = perf;
    }
    public Integer getPerf()
    {
        return this.perf;
    }
    public static final Creator<Exercise> CREATOR = new Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel in) {
            return new Exercise(in);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public short getReps() {
        return reps;
    }

    public Integer getRestDuration() {
        return restDuration;
    }

    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setReps(short reps) {
        this.reps = reps;
    }

    public void setRestDuration(Integer restDuration) {
        this.restDuration = restDuration;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(description);
        parcel.writeInt((int) reps);
        if (restDuration == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(restDuration);
        }
        parcel.writeString(exerciseType.name());
    }

    public Integer getIconId() {
        return iconId;
    }
    @Override
    public String toString() {
        return name;
    }
}
