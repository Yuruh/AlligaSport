package ca.ulaval.ima.mp.music;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yuruh on 30/03/18.
 */

public class SpotifyApi {

    private OkHttpClient client = new OkHttpClient
            .Builder()
            .build();

    private String endpoint = "https://api.spotify.com/v1";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static SpotifyApi instance = new SpotifyApi();
    private String token = null;
    private String userId = "";

    private SpotifyApi() {
    }

    public static SpotifyApi getInstance() {
        return instance;
    }

    private Call get(String url, Callback callback) {
        url = this.endpoint + url;
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + this.token)
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);

        return call;
    }

    private Call post(String url, String json, Callback callback) {
        url = this.endpoint + url;
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + this.token)
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);

        return call;
    }

    private Call put(String url, String json, Callback callback) {
        url = this.endpoint + url;
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + this.token)
                .url(url)
                .put(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);

        return call;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setupUserId() {
        this.getMe(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONObject object = new JSONObject(response.body().string());
                    userId = object.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Call getMe(Callback callback) {
        return this.get("/me/", callback);
    }

    public Call getTopTracks(Callback callback) {
        return this.get("/me/top/tracks", callback);
    }

    public Call getPlaylists(Callback callback) {
        return this.get("/me/playlists", callback);
    }

    public Call getPlaylistTracks(String id, Callback callback) {
        return this.get("/users/" + userId + "/playlists/" + id + "/tracks", callback);
    }

    public Call addPlaylist(String name, Callback callback) {
        JSONObject  body = new JSONObject();
        try {
            body.put("name", name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this.post("/users/" + userId + "/playlists", body.toString(), callback);
    }

    public Call getTracks(Callback callback) {
        return this.get("/me/tracks", callback);
    }

    public void pause(Callback callback) {
        this.put("/me/player/pause", "", callback);
    }

    public void play(Callback callback) {
        this.put("/me/player/play", "", callback);
    }

    public void prev(Callback callback) {
        this.post("/me/player/previous", "", callback);
    }

    public void next(Callback callback) {
        this.post("/me/player/next", "", callback);
    }


}
