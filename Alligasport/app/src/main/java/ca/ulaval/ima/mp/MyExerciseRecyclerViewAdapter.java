package ca.ulaval.ima.mp;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ca.ulaval.ima.mp.DuringSessionFragment.OnListFragmentInteractionListener;

import java.util.List;


public class MyExerciseRecyclerViewAdapter extends RecyclerView.Adapter<MyExerciseRecyclerViewAdapter.ViewHolder> {

    private final List<Exercise> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Fragment mCtxFragment;
    public static int EXERCICE_DESC_REQUEST_CODE = 1010;

    public MyExerciseRecyclerViewAdapter(List<Exercise> exercises, OnListFragmentInteractionListener listener,
                                         Fragment ctxFragment) {
        mValues = exercises;
        mListener = listener;
        mCtxFragment = ctxFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_exercise, parent, false);
        return new ViewHolder(view);
    }

    public void removeElem(Exercise elem) {
        for (int i = 0; i < mValues.size(); i++) {
            if (mValues.get(i).getId() == elem.getId()) {
                //Log.wtf("azer", "azer");
                mValues.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, mValues.size());
                break;
            }
        }
    }

    public boolean isArrayEmpty() {
        return mValues.isEmpty();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Exercise item = mValues.get(position);
        holder.mItem = item;
        holder.mNameView.setText(Integer.toString(item.getReps()) + " reps - " + Integer.toString(item.getRestDuration()) + " sec of rest");
        holder.mContentView.setText(item.getName());
        holder.mImageView.setImageResource(item.getIconId());

        String nom;
        Integer rep;
        Integer tps;

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                    String desc = holder.mItem.getDescription();
                    String nom = holder.mItem.getName();
                    int id = holder.mItem.getId();
                    short rep = holder.mItem.getReps();
                    int rest = holder.mItem.getRestDuration();
                    ExerciseType exerciseType = holder.mItem.getExerciseType();

                    Exercise myExercise = new Exercise(nom,id,desc,rep,rest,exerciseType);
                    Context c = holder.mView.getContext();
                    Intent myIntent = new Intent(c,DescriptionActivity.class);
                    myIntent.putExtra("exercise",myExercise);
                    mCtxFragment.startActivityForResult(myIntent, EXERCICE_DESC_REQUEST_CODE);
                    //Toast.makeText(holder.mView.getContext(),holder.mItem.getName(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mContentView;
        public final ImageView mImageView;
        public Exercise mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.name_exercise);
            mContentView = (TextView) view.findViewById(R.id.content_exercise);
            mImageView = (ImageView) view.findViewById(R.id.imageView_exercise);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
