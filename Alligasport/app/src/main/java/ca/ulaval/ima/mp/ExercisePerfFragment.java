package ca.ulaval.ima.mp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ExercisePerfFragment extends Fragment {

    public ExercisePerfFragment() {
    }

    private  String exerciseName = null;

    @SuppressWarnings("unused")
    public static ExercisePerfFragment newInstance(String exercise) {
        ExercisePerfFragment fragment = new ExercisePerfFragment();
        Bundle args = new Bundle();
        args.putString("EXERCISE_NAME", exercise);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            exerciseName = getArguments().getString("EXERCISE_NAME");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exerciseperf_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            ArrayList<ExercisePerf> perfArrayList = new ArrayList<>();
            try {
                JSONArray array = FileManager.getInstance().getPerfs().getJSONArray("exercises");
                for (int i = 0; i < array.length(); ++i)
                {
                 if (((JSONObject)array.get(i)).optString("exercise_name").matches(exerciseName))
                 {
                     JSONArray perfArrayJson = ((JSONObject) array.get(i)).getJSONArray("perf");
                     for (int j = 0; j < perfArrayJson.length(); ++j)
                     {
                         perfArrayList.add(new ExercisePerf(perfArrayJson.get(j).toString()));
                     }

                 }
                }
                recyclerView.setAdapter(new ExercisePerfRecyclerViewAdapter(perfArrayList));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return view;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   // public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        //void onListFragmentInteraction(ExercisePerf item);
   // }
    public class ExercisePerf {
        public final String exercisePerf;

        public ExercisePerf(String exercisePerf) {
            this.exercisePerf = exercisePerf;
        }
    }
}
