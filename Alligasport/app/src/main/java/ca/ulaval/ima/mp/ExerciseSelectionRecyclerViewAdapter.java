package ca.ulaval.ima.mp;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ca.ulaval.ima.mp.ExerciseSelectionFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Exercise} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class ExerciseSelectionRecyclerViewAdapter extends RecyclerView.Adapter<ExerciseSelectionRecyclerViewAdapter.ViewHolder> {

    private final List<Exercise> mValues;
    private final OnListFragmentInteractionListener mListener;

    public ExerciseSelectionRecyclerViewAdapter(List<Exercise> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_exercise_selection, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mExerciseName.setText(mValues.get(position).getName());
        switch (mValues.get(position).getExerciseType()) {
            case WEIGHT:
                holder.mExerciseTypeImage
                        .setImageResource(R.drawable.ic_weight);
                break;
            case CARDIO:
                holder.mExerciseTypeImage
                        .setImageResource(R.drawable.ic_cardio);
                break;
        }
        if (holder.mSelected)
            holder.itemView.setBackgroundColor(Color.GREEN);
        else
            holder.itemView.setBackgroundColor(Color.WHITE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    if (holder.mSelected) {
                        holder.itemView.setBackgroundColor(Color.WHITE);
                        holder.mSelected = false;
                        holder.mItem.selected = false;
                    }
                    else {
                        holder.mSelected = true;
                        holder.itemView.setBackgroundColor(Color.parseColor("#00bf0b"));
                        holder.mItem.selected = true;
                    }

                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    public List<Exercise> getItems() {
        return mValues;
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mExerciseName;
        public final ImageView mExerciseTypeImage;
        public boolean mSelected;
        public Exercise mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mSelected = false;
            mExerciseName = view.findViewById(R.id.session_name_text);
            mExerciseTypeImage = view.findViewById(R.id.icon_type_ImageView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mExerciseName.getText() + "'";
        }
    }
}
