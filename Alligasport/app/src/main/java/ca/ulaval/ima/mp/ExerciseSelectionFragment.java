package ca.ulaval.ima.mp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ExerciseSelectionFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private ExerciseSelectionRecyclerViewAdapter adapter = null;
    public static final List<Exercise> exerciseList = new ArrayList<Exercise>();


    public ExerciseSelectionFragment() {
    }

    public static ExerciseSelectionFragment newInstance(int columnCount) {
        ExerciseSelectionFragment fragment = new ExerciseSelectionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    private static Exercise createExerciseItem(int id, String name, String type,
                                               Integer restDuration, short reps,
                                               String description) {
        return new Exercise(name, id,description, reps, restDuration, ExerciseType.valueOf(type));
    }

    public void initializeList() throws JSONException {
        JSONObject exercises = FileManager.getInstance().getExercises();
        JSONArray exercisesArray = exercises.getJSONArray("exercises");
        exerciseList.clear();
        for (int i = 0; i < exercisesArray.length(); i++)
        {
            JSONObject obj = exercisesArray.getJSONObject(i);
            exerciseList.add(createExerciseItem(obj.getInt("id"),
                    obj.getString("name"),
                    obj.getString("type"),
                    obj.getInt("rest_duration"),
                    (short) obj.getInt("reps"),
                    obj.getString("description")));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Select Exercises");

        View view = inflater.inflate(R.layout.fragment_exercise_selection_list, container, false);


        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            adapter = new ExerciseSelectionRecyclerViewAdapter(exerciseList, mListener);
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    public List<Exercise> getExerciseItemsList() {
        return adapter.getItems();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Exercise item);
    }


}
