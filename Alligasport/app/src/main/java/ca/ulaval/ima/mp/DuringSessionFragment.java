package ca.ulaval.ima.mp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import static android.app.Activity.RESULT_OK;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DuringSessionFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private MyExerciseRecyclerViewAdapter   adapter;
    private Session session;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DuringSessionFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DuringSessionFragment newInstance(Session session) {
        DuringSessionFragment fragment = new DuringSessionFragment();
        Bundle args = new Bundle();
        args.putParcelable("SESSION", session);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getArguments() != null) {
            session = getArguments().getParcelable("SESSION");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(this.session.getName());
//        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        View view = inflater.inflate(R.layout.fragment_exercise_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            adapter = new MyExerciseRecyclerViewAdapter(new Session(session).getExercises(), mListener, this);
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MyExerciseRecyclerViewAdapter.EXERCICE_DESC_REQUEST_CODE && resultCode == RESULT_OK) {
            adapter.removeElem((Exercise) data.getParcelableExtra("EXERCISE"));
        }

        if (adapter.isArrayEmpty())
        {
            Intent intent = new Intent(this.getContext(), PerfActivity.class);
            intent.putExtra("SESSION", session);
            startActivity(intent);
            ((MainActivity)getActivity()).endDuringSession();
            this.getFragmentManager().popBackStackImmediate();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Exercise item);
    }
}
