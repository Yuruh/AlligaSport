package ca.ulaval.ima.mp.music;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;

import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerEvent;
import com.spotify.sdk.android.player.SpotifyPlayer;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by yuruh on 12/04/18.
 */

public class MusicHandler implements Player.NotificationCallback, ConnectionStateCallback, MediaController.MediaPlayerControl {

    private Player mPlayer;
    private MediaController mediaController;
    private static MusicHandler instance = new MusicHandler();
    private Queue<SpotifyTrack> customQueue = new LinkedBlockingQueue<>();


    public static final Player.OperationCallback callback = new Player.OperationCallback() {
        @Override
        public void onSuccess() {
            Log.w("MusicHandler", "OK!");
        }

        @Override
        public void onError(Error error) {
            Log.e("MusicHandler", "ERROR:" + error);
        }
    };


    private MusicHandler() {
        mPlayer = null;
    }

    public static MusicHandler getInstance() {
        return instance;
    }

    public void addToQueue(SpotifyTrack track) {
        this.customQueue.add(track);
    }

    //pô sur que c'est bien
    public void setupMediaController(View view, Context context) {
        mediaController = new MediaController(context);
        mediaController.setMediaPlayer(this);
        mediaController.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPlayer.skipToNext(callback);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPlayer.skipToPrevious(callback);
            }
        });
        mediaController.setAnchorView(view);
    }

    public void showMediaController(View view) {
        if (!mediaController.isShowing()) {
            mediaController.setAnchorView(view);
        }
        mediaController.show();
    }
    public void hideMediaController() {
        mediaController.hide();
    }



    public void playUri(String uri) {
        mPlayer.playUri(new Player.OperationCallback() {
            @Override
            public void onSuccess() {
                mediaController.show(0);
            }

            @Override
            public void onError(Error error) {
                mediaController.show(0);
            }
        }, uri, 0, 0);
        mediaController.show();
    }

    public void setPlayer(SpotifyPlayer player) {
        this.mPlayer = player;
        this.mPlayer.addConnectionStateCallback(MusicHandler.this);
        this.mPlayer.addNotificationCallback(MusicHandler.this);
    }

    @Override
    public void onLoggedIn() {
        Log.w("MusicFragment", "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.w("MusicFragment", "User logged out");

    }

    @Override
    public void onLoginFailed(Error error) {
        Log.w("MusicFragment", "Login failed");

    }

    @Override
    public void onTemporaryError() {
        Log.w("MusicFragment", "Temporary error occurred");

    }

    @Override
    public void onConnectionMessage(String s) {
        Log.w("MusicFragment", "Received connection message: " + s);

    }

    @Override
    public void onPlaybackEvent(PlayerEvent playerEvent) {
        Log.w("MusicFragment", "Playback event received: " + playerEvent.name());
        switch (playerEvent) {
//            case kSpPlaybackNotifyNext:
            case kSpPlaybackNotifyBecameActive:
                this.mPlayer.setShuffle(callback, true);
//                mPlayer.
                break;

            case kSpPlaybackNotifyTrackChanged:
                if (customQueue != null && !customQueue.isEmpty())
                    this.mPlayer.queue(callback, customQueue.remove().uri);
                break;

            case kSpPlaybackNotifyMetadataChanged:
                Log.wtf("metadata", String.valueOf(mPlayer.getMetadata().nextTrack));
                break;
                 // Handle event type as necessary
            default:
                break;
        }
    }

    @Override
    public void onPlaybackError(Error error) {
        Log.e("MusicFragment", "Playback error received: " + error.name());
        switch (error) {
            // Handle error type as necessary
            default:
                break;
        }
    }


    public Player getPlayer() {
        return mPlayer;
    }

    @Override
    public void start() {
        this.mPlayer.resume(callback);
    }

    @Override
    public void pause() {
        this.mPlayer.pause(callback);
    }

    @Override
    public int getDuration() {
        if (mPlayer == null || mPlayer.getMetadata().currentTrack == null)
            return 0;
        return (int) mPlayer.getMetadata().currentTrack.durationMs;
    }

    @Override
    public int getCurrentPosition() {
        if (mPlayer == null || mPlayer.getMetadata().currentTrack == null)
            return 0;
        return (int) mPlayer.getPlaybackState().positionMs;
    }

    @Override
    public void seekTo(int i) {
        mPlayer.seekToPosition(null, i);
    }

    @Override
    public boolean isPlaying() {
        if (mPlayer == null || mPlayer.getPlaybackState() == null)
            return false;
        return mPlayer.getPlaybackState().isPlaying;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    public void clearQueue() {
        customQueue.clear();
    }
}