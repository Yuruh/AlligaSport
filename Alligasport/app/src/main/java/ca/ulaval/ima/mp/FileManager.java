package ca.ulaval.ima.mp;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by cot_s on 18-04-11.
 */

public class FileManager {
    private static FileManager INSTANCE = new FileManager();
    private File exercisesFile = null;
    private File perfFile = null;
    private File sessionsFile = null;
    private File directoryPath = null;
    private JSONObject exercises = null;
    private JSONObject perfs = null;
    private JSONObject sessions = null;


    private FileManager() {
    }



    public static FileManager getInstance() {   return INSTANCE; }

    public static FileManager getInstance(Context context) {
        INSTANCE.directoryPath = context.getFilesDir();
        INSTANCE.exercisesFile = new File(INSTANCE.directoryPath, "exercisesjson");
        INSTANCE.perfFile = new File(INSTANCE.directoryPath, "perfsjson");
        INSTANCE.sessionsFile = new File(INSTANCE.directoryPath, "sessionsJson");
        if (!INSTANCE.exercisesFile.exists())
            INSTANCE.writeToFile(INSTANCE.getJsonStringFromRaw(context, R.raw.exercisesjson), INSTANCE.exercisesFile);
        if (!INSTANCE.sessionsFile.exists())
            INSTANCE.writeToFile(INSTANCE.getJsonStringFromRaw(context, R.raw.sessionjson), INSTANCE.sessionsFile);
        if (!INSTANCE.perfFile.exists())
            INSTANCE.writeToFile(INSTANCE.getJsonStringFromRaw(context, R.raw.perfsjson), INSTANCE.perfFile);
        return INSTANCE;
    }


    private void writeToFile(String data, File file) {

        FileOutputStream stream = null;
        try {
            file.createNewFile();
            stream = new FileOutputStream(file);
            stream.write(data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String readFromFile(File file) throws IOException {

        int length = (int) file.length();

        byte[] bytes = new byte[length];
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            in.read(bytes);
        } finally {
            in.close();
        }
        return new String(bytes);
    }

    public void reWriteSessionsFile(JSONObject jsonObject) {
        writeToFile(jsonObject.toString(), this.sessionsFile);
    }

    public void writeExercisesFile(JSONObject jsonObject) {
        writeToFile(jsonObject.toString(), this.exercisesFile);
    }

    public void reWritePerfFile(JSONObject jsonObject) {
        writeToFile(jsonObject.toString(), this.perfFile);
    }

    public JSONObject getPerfs() {
        return perfs;
    }

    public JSONObject getExercises() {
        return exercises;
    }

    public JSONObject getSessions() {
        return sessions;
    }

    public void setPerfs(JSONObject obj) {
        perfs = obj;
    }

    public void setSessions(JSONObject obj) {
        sessions = obj;
    }

    public void setAndRewriteSessionFile(JSONObject obj) {
        this.setSessions(obj);
        this.reWriteSessionsFile(obj);
    }

    public void setAndRewritePerfFile(JSONObject obj) {
        this.setPerfs(obj);
        this.reWritePerfFile(obj);
    }

    private String getJsonStringFromRaw(Context context, int res_id) {
        InputStream inputStream = context.getResources().openRawResource(res_id);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1)
            {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return byteArrayOutputStream.toString();
    }

    public void updateJsonFiles() {
            try {
                if (exercisesFile.exists())
                    exercises = new JSONObject(readFromFile(exercisesFile));
                if (perfFile.exists())
                    perfs = new JSONObject(readFromFile(perfFile));
                if (sessionsFile.exists())
                    sessions = new JSONObject(readFromFile(sessionsFile));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

    }


}
