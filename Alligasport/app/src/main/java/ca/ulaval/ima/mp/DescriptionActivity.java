package ca.ulaval.ima.mp;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DescriptionActivity extends AppCompatActivity {

    private TextView mTextName;
    private TextView mTextRest;
    private TextView mTextRep;
    private TextView mTextDesc;
    private Button compteRebours;
    private TextView mTextTemps;
    private int cptRep;

    private Fragment musicFragment;
    private Fragment sessionFragment;
    private Fragment createSessionFragment;
    private Fragment speedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        mTextName = (TextView) findViewById(R.id.name);
        mTextRest = (TextView) findViewById(R.id.time);
        mTextRep = (TextView) findViewById(R.id.rep);
        mTextDesc = (TextView) findViewById(R.id.desc);
        mTextTemps = (TextView) findViewById(R.id.temps);
        cptRep = 0;


        final Exercise myExercise = getIntent().getExtras().getParcelable("exercise");
        mTextName.setText(myExercise.getName());
        mTextRest.setText("Temps de récupération : "+myExercise.getRestDuration() +" secondes");
        mTextRep.setText("Nombre de répétitions : "+myExercise.getReps());
        mTextDesc.setText("Description : "+myExercise.getDescription());
        mTextTemps.setText("Temps de récupération : "+myExercise.getRestDuration() +" secondes");


        Button exoDone = findViewById(R.id.button_doneExo);
        exoDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                data.putExtra("EXERCISE", myExercise);
                setResult(RESULT_OK, data);
                finish();
            }
        });

        final CountDownTimer countDownTimer = new CountDownTimer(myExercise.getRestDuration()*1000, 1000) {
            @Override
            public void onTick(long l) {
                mTextTemps.setText("Seconds remaining : "+ l / 1000 +" "+ cptRep +"/"+myExercise.getReps());
            }

            @Override
            public void onFinish() {
                if(cptRep > myExercise.getReps()){
//                    mTextTemps.setText("This exercise is over, congrat");
                    Intent data = new Intent();
                    data.putExtra("EXERCISE", myExercise);
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    mTextTemps.setText("Done !" + cptRep+"/"+myExercise.getReps());
                }
            }
        };
        compteRebours = (Button) findViewById(R.id.compteRebours);
        compteRebours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cptRep > myExercise.getReps()){
                    mTextTemps.setText("This exercise is over, congrat");
                }else {
                    countDownTimer.start();
                    cptRep++;
                }
            }
        });

                //Toast.makeText(this,myExercise.getName(),Toast.LENGTH_LONG).show();

    }

}
