package ca.ulaval.ima.mp.music;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ca.ulaval.ima.mp.R;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Player;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link SpotifyTrack} and makes a call to the
 * specified {@link TrackListFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class TrackRecyclerViewAdapter extends RecyclerView.Adapter<TrackRecyclerViewAdapter.ViewHolder> {

    private final List<SpotifyTrack> mValues;
    private final TrackListFragment.OnListFragmentInteractionListener mListener;
    private final TrackListFragment.OnTrackClickListener mTrackListener;
    private int   selected = -1;

    public TrackRecyclerViewAdapter(List<SpotifyTrack> items,
                                    TrackListFragment.OnListFragmentInteractionListener listener,
                                    TrackListFragment.OnTrackClickListener trackListener) {
        mValues = items;
        mListener = listener;
        mTrackListener = trackListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_track, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        SpotifyTrack item = mValues.get(position);
        holder.mItem = item;
        holder.mTrackView.setText(item.name);
        String album_artist = item.artist + " - "  + item.album;
        holder.mAlbumView.setText(album_artist);
//        if (position == selected)
//            holder.mView.setBackgroundColor(ContextCompat.getColor(holder.mView.getContext(), R.color.colorTrackOdd));
/*        if (position % 2 == 1)
            holder.mView.setBackgroundColor(ContextCompat.getColor(holder.mView.getContext(), R.color.colorTrackOdd));
        else
            holder.mView.setBackgroundColor(ContextCompat.getColor(holder.mView.getContext(), R.color.colorTrackEven));*/
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                    final List<SpotifyTrack> spotifyTracks = mValues.subList(position + 1, mValues.size());

                    final Player player = MusicHandler.getInstance().getPlayer();

                    player.playUri(new Player.OperationCallback() {
                        @Override
                        public void onSuccess() {
                            MusicHandler.getInstance().clearQueue();
                            for (SpotifyTrack nextTrack : spotifyTracks) {
                                MusicHandler.getInstance().addToQueue(nextTrack);
                            }
                        }

                        @Override
                        public void onError(Error error) {

                        }
                    }, holder.mItem.uri, 0, 0);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTrackView;
        public final TextView mAlbumView;
        public SpotifyTrack mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTrackView = view.findViewById(R.id.text_track);
            mAlbumView = view.findViewById(R.id.text_album_artist);
        }
    }
}
