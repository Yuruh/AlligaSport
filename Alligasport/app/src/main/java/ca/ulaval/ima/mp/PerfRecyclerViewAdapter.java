package ca.ulaval.ima.mp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PerfRecyclerViewAdapter extends RecyclerView.Adapter<PerfRecyclerViewAdapter.ViewHolder> {

    private final List<Exercise> mValues;
    private final PerfFragment.OnListFragmentInteractionListener mListener;

    public PerfRecyclerViewAdapter(List<Exercise> items, PerfFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_perf, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNameView.setText(mValues.get(position).getName());
        holder.mPerfView.setText(mValues.get(position).getPerf().toString());
        //holder.mExerciseQtyView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mPerfView;

        public Exercise mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.perf_exercise_name);
            mPerfView = (TextView) view.findViewById(R.id.perf_number);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}
